from flask import Flask

app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

@app.route("/yo")
def yo():
    return "<h1>yo wassup</h1>"